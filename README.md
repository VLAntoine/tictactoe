# Jeu du Morpion et Gobblets en Vuejs

## Utiliser l'application

Cloner le projet, installer les dépendances :

```
npm install
```

Lancer le projet :

```
npm run serve
```

## Règles des jeux

### Morpion


#### BUT DU JEU :
  <p>Aligner ses 3 symboles sur une ligne, une colonne ou une diagonale.</p>

#### DÉROULEMENT D’UNE PARTIE :

  <p>A tour de rôle, chaque joueur place sur une des cases de la grille son symbole.</p>

#### FIN DE LA PARTIE :

  <p>Dès qu’un joueur a  ses 3 symboles alignés, il est déclaré vainqueur.</p>
  <p>Si la grille est remplie sans qu'il n'y ait de gagnant, il y a égalité.</p>

### Gobblets

#### BUT DU JEU :
  Aligner 4 GOBBLETS de sa couleur.
  
#### DÉROULEMENT D’UNE PARTIE :
  <p>A tour de rôle, chaque joueur peut :</p>
  <ul>
    <li>
      Soit mettre en jeu un nouveau GOBBLET dans un espace vide
    </li>
    <li>
      Soit déplacer un de ses GOBBLETS déjà en jeu dans un espace vide ou pour gober une pièce de taille inférieure
    </li>
    </ul>
    <p> Un nouveau GOBBLET mis en jeu doit obligatoirement être placé sur une des cases vides, sauf si l’adversaire a
    3 GOBBLETS alignés : dans ce seul cas, on peut prendre un GOBBLET à l’extérieur du plateau pour gober une de ces 3 pièces.
    Une fois engagé, un GOBBLET ne peut être ressorti du jeu.
    Si un joueur a placé toutes ses pièces avant la fin de la partie, il continue ses déplacements jusqu’à la victoire d’un des 2 joueurs.</p>
  

#### FIN DE LA PARTIE :

  <p>Dès qu’un joueur a 4 GOBBLETS de sa couleur alignés, quelque soit leur taille, verticalement, horizontalement ou en diagonale, il gagne la partie.</p>


## Contexte

Ce travail est le produit d'un devoir demandé lors d'un cours sur Javascript et ces frameworks de B3 à l'EPSI lors de l'année scolaire
2022-2023. L'objectif était de réaliser un jeu de Morpion en utilisant le maximum des fonctionnalités de Vuejs. On pouvait pousser l'exercice plus
loin en réalisant un Gobblets.

## Implémentation

Le jeu du morpion n'utilise pas le _store_. Il est organisé autour de 2 composants Vue :

- _TicTacToeGame_ : contient toutes la logique du jeu, détermine qui est vainqueur, passe la main au joueur suivant, etc.
- _TicTacToeBox_ : contient l'affichage d'une case du jeu.

Le Gobblets utilise le store, qui contient tout l'état du jeu et permet d'effectuer les mutations de cet état.
Il contient 4 éléments qui gèrent l'affichage du jeu et font les appels aux getters et mutations du store (GobbletsGameBoard, GobbletsGameGrid, GobbletsPlayerStacks et GobbletsBox).

La navigation entre les différentes pages s'effectuent grâce au router.

