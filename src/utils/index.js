export function getGobbletMax(gobblets){
    let gobbletMax = {};

    if (gobblets.length > 0) {
        gobbletMax = gobblets.reduce(
            (acc, currentGobblet) => acc.size > currentGobblet.size ? acc : currentGobblet,
            gobblets[0]
        )
    }
    return gobbletMax;
}