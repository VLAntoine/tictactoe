import {createStore} from "vuex";
import {getGobbletMax} from "@/utils";
import {isEqual} from "lodash";

export const store = createStore({
    state() {
        return {
            gobbletsGridSide: 4,
            nbOfGobbletsDifferentSizes: 4,
            nbOfStacksPerPlayer: 3,
            gobbletSprite: "\u{2b24}",
            players: [
                {
                    name: "Joueur 1",
                    color: "red",
                },
                {
                    name: "Joueur 2",
                    color: "white"
                }
            ],
            boxChosen: {},
            currentPlayerIndex: 0,
            gobbletGameBoard: {
                grid: [],
                stacks: []
            },
            winnerIndex: -1
        }
    },
    getters: {

        /**
         * Retourn true si la case choisie (state.boxChosen) est égale à celle donnée en paramètre.
         */
        isBoxChosen: (state) => (box) => {
            return isEqual(state.boxChosen, box);
        },

        /**
         * Retourne la couleur du plus grand gobelet de la case donnée en paramètre ('' s'il n'y a pas de gobelet)
         */
        color: (state) => (box) => {
            let gobbletMax = {};
            if ("stackIndex" in box){
                gobbletMax = getGobbletMax(state.gobbletGameBoard.stacks[box.playerIndex][box.stackIndex]);
            }
            else {
                gobbletMax = getGobbletMax(state.gobbletGameBoard.grid[box.row][box.col]);
            }

            return "playerIndex" in gobbletMax ? state.players[gobbletMax.playerIndex].color : "";
        },

        /**
         * Retourne la taille du plus grand gobelet de la case donnée en paramètre (-1 s'il n'y a pas de gobelet)
         */
        size: (state) => (box) => {
            let gobbletMax = {};
            if ("stackIndex" in box){
                gobbletMax = getGobbletMax(state.gobbletGameBoard.stacks[box.playerIndex][box.stackIndex]);
            }
            else {
                gobbletMax = getGobbletMax(state.gobbletGameBoard.grid[box.row][box.col]);
            }
            return "size" in gobbletMax ? gobbletMax.size : -1;
        },

        /**
         * Retourne le joueur courant
         */
        currentPlayer(state) {
            return state.players[state.currentPlayerIndex];
        },

        /**
         * Retourne true si on peut déplacer le gobelet max de la case choisie ou si on peut le déplacer vers cette case.
         */
        isDisabled: (state, getters) => (box) => {
            return state.winnerIndex !== -1
            || (Object.keys(state.boxChosen).length === 0 ? !getters.isPlayableFrom(box) : !getters.isPlayableTo(box));
        },

        /**
         * Retourne true si on peut jouer le plus haut gobelet de la case choisie (state.chosenGobblet) vers cette case.
         */
        isPlayableTo: (state, getters) => (box) => {
            let playableTo = false;

            // si la case donnée en paramètre n'est pas dans la réserve du joueur
            if (!("stackIndex" in box)) {
                const boxGobblets = state.gobbletGameBoard.grid[box.row][box.col];
                const boxChosenGobblets = getters.getBoxChosenGobblets;
                const gobbletMax = getGobbletMax(boxGobblets);

                // On peut déplacer le gobelet choisi vers la case en paramètre si :
                //  - il n'y a aucun gobelet sur cette case
                playableTo = (boxGobblets.length === 0
                //  - OU la taille du gobelet choisi est strictement plus grande que celle du plus grand gobelet de cette case
                    || (getGobbletMax(boxChosenGobblets).size
                        > gobbletMax.size
                        // ET
                        // - la case choisie précédemment ne fait pas partie de la réserve d'un joueur
                        && ((!("stackIndex" in state.boxChosen))
                        // - OU il s'agit d'une case déjà dominée par le joueur
                            || gobbletMax.playerIndex === state.currentPlayerIndex
                        // - OU le joueur adverse a déjà un alignement de 3 cases (côté de la grille - 1)
                            || getters.isOpponentWinning)));
            }
            return playableTo;
        },

        /**
         * Retourne true si on peut jouer le plus haut gobelet de la case en paramètre vers une autre case.
         */
        isPlayableFrom: (state) => (box) => {
            let boxGobblets = [];
            if ("stackIndex" in box) {
                boxGobblets = state.gobbletGameBoard.stacks[box.playerIndex][box.stackIndex];
            }
            else {
                boxGobblets = state.gobbletGameBoard.grid[box.row][box.col];
            }
            // retourne true si :
            // - il y a des gobelets sur la case donnée en paramètre
            return boxGobblets.size !== 0
            // - le plus grand gobelet appartient au joueur dont c'est le tour
                && getGobbletMax(boxGobblets).playerIndex === state.currentPlayerIndex;
        },

        /**
         * Retourne la liste des gobelets de la case choisie (state.boxChosen)
         */
        getBoxChosenGobblets(state){
            let gobblets = [];
            if ("stackIndex" in state.boxChosen) {
                gobblets = state.gobbletGameBoard.stacks[state.currentPlayerIndex][state.boxChosen.stackIndex];
            }
            else {
                gobblets = state.gobbletGameBoard.grid[state.boxChosen.row][state.boxChosen.col];
            }
            return gobblets;
        },

        /**
         * Retourne true si le joueur adverse a déjà un alignement de 3 cases (côté de la grille - 1)
         */
        isOpponentWinning(state){
            let isOpponentWinning = false;

            const gameGrid = state.gobbletGameBoard.grid;
            // la liste de toutes les colonnes, grilles et diagonales du jeu
            let gridLines = [];

            // l'indice de l'adversaire est celui du joueur + 1
            const opponentIndex = (state.currentPlayerIndex + 1) % state.players.length;

            // ajoute la diagonale NO-SE à la liste des rangs qui composent la grille
            gridLines.push(gameGrid.map((value, index) => value[index]));
            // ajoute la diagonale NE-SO à la liste des rangs qui composent la grille
            gridLines.push(gameGrid.map((value, index) => value[state.gobbletsGridSide - index - 1]));

            // ajoute toutes les colonnes et toutes les lignes à la liste des rangs qui composent la grille
            for (let i=0; i<state.gobbletsGridSide; i++){
                gridLines.push(gameGrid[i]);
                gridLines.push(gameGrid.map(value => value[i]));
            }

            // pour chacun des rangs de la grille
            for (const arr of gridLines){
                // si la ligne contient 3 éléments (côté de la grille - 1) dominés par le joueur adverse, la condition est remplie et stoppe la boucle
                if (arr.reduce((acc, gobblets) => getGobbletMax(gobblets).playerIndex === opponentIndex ? acc+1 : acc, 0) === state.gobbletsGridSide - 1){
                    isOpponentWinning = true;
                    break;
                }
            }

            return isOpponentWinning;
        }
    },
    mutations: {
        /**
         * Le setter des stacks de la partie de Gobblets
         */
        setStacks(state, stacks){
            state.gobbletGameBoard.stacks = stacks;
        },

        /**
         * Le setter de la grille de la partie de Gobblets
         */
        setGrid(state, grid){
          state.gobbletGameBoard.grid = grid;
        },

        setWinnerIndex(state, winnerIndex){
            state.winnerIndex = winnerIndex;
        },

        setCurrentPlayerIndex(state, currentPlayerIndex){
            state.currentPlayerIndex = currentPlayerIndex;
        },

        /**
         * Choisit la case de laquelle on prend le plus grand gobelet si ce n'est pas encore fait,
         * sinon joue vers la case donnée en paramètre.
         */
        play(state, box){
            // s'il n'y a pas de case choisie, le fait
            if (Object.keys(state.boxChosen).length === 0) {
                store.commit("setBoxChosen", box);
            }
            // sinon déplace le plus haut gobelet de la case choisie
            else {
                store.commit("move", box);
            }
        },
        /**
         * Le setter de la case choisie
         */
        setBoxChosen(state, boxChosen) {
            state.boxChosen = boxChosen;
        },

        /**
         * Déplace le gobelet de la case choisie (state.boxChosen) vers la case donnée en paramètre.
         */
        move(state, box){

            let movingGobblet = {};

            /*
             retire le plus haut gobelet de la case choisie et l'enregistre dans movingGobblet
             */

            if ("stackIndex" in state.boxChosen){
                movingGobblet =
                    state.gobbletGameBoard.stacks[state.currentPlayerIndex][state.boxChosen.stackIndex].pop()
            }

            // si la case choisie ne fait pas partie de la réserve d'un joueur
            else {
                const boxChosenRow = state.boxChosen.row;
                const boxChosenCol = state.boxChosen.col;
                movingGobblet =
                    state.gobbletGameBoard.grid[boxChosenRow][boxChosenCol].pop();
                // teste la victoire après retrait du gobelet (si le fait de l'enlever, fait gagner un joueur) et donne
                // le vainqueur le cas échéant
                store.commit("checkWin", {row: boxChosenRow, col: boxChosenCol});
            }

            // s'il n'y a pas de gagnant suite au dévoilement du gobelet
            if (state.winnerIndex === -1) {
                // ajoute le gobelet à la case donnée en paramètre
                state.gobbletGameBoard.grid[box.row][box.col].push(movingGobblet);
                // teste la victoire après le déplacement
                store.commit("checkWin", {row: box.row, col: box.col});
                // passe la main au joueur suivant
                store.commit("nextPlayer");
                // réinitialise la boite choisie
                store.commit("setBoxChosen", {})
            }

        },

        /**
         * Passe la main au joueur suivant.
         */
        nextPlayer(state) {
            // incrémente state.currentPlayerIndex
            state.currentPlayerIndex = (state.currentPlayerIndex + 1) % state.players.length;
        },

        /**
         * Teste la victoire.
         * Si un joueur est victorieux, modifie l'état de la partie.
         */
        checkWin(state, box){
            const gameGrid = state.gobbletGameBoard.grid;

            // si la case donnée en paramètre  n'est pas dans la réserve d'un joueur et si elle n'est pas vide
            if (!("stackIndex" in box) && (gameGrid[box.row][box.col].length !== 0)) {

                const playerIndex = getGobbletMax(gameGrid[box.row][box.col]).playerIndex;

                // la ligne sur laquelle se trouve la case donnée en paramètre
                const rowArray = gameGrid[box.row];
                // la colonne sur laquelle se trouve la case donnée en paramètre
                const colArray = gameGrid.map(value => value[box.col]);
                // la diagonale NO-SE
                const diagNWSE = gameGrid.map((value, index) => value[index])
                // la diagonale NE-SO
                const diagNESW = gameGrid.map((value, index) => value[state.gobbletsGridSide - index - 1]);

                // pour chacun des 4 rangs,
                for (const arr of [rowArray, colArray, diagNWSE, diagNESW]){
                    // si la ligne contient 4 éléments (côté de la grille - 1) dominés par un même joueur,
                    if (arr.reduce((acc, gobblets) => getGobbletMax(gobblets).playerIndex === playerIndex ? acc+1 : acc, 0) === state.gobbletsGridSide){
                        // met à jour l'indice du gagnant
                        state.winnerIndex = playerIndex;
                        // réinitialise la case choisie
                        store.commit("setBoxChosen", {})
                        break;
                    }
                }
            }
        },
    }
})