import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import GobbletsGameMenu from "@/views/GobbletsGameMenu.vue";
import TicTacToeMenu from "@/views/TicTacToeMenu.vue";
import GobbletsRules from "@/components/GobbletsRules.vue";
import TicTacToeRules from "@/components/TicTacToeRules.vue";

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/gobblets',
    name: 'gobbletsGameMenu',
    component:GobbletsGameMenu
  },
  {
    path: '/gobblets/about',
    name: 'gobbletsRules',
    component: GobbletsRules
  },
  {
    path: '/tic-tac-toe',
    name: 'ticTacToeMenu',
    component: TicTacToeMenu
  },
  {
    path: '/tic-tac-toe/about',
    name: 'ticTacToeRules',
    component: TicTacToeRules
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
